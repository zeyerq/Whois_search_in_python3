#!/usr/bin/env python3.11

import sys
try:
    from whois import whois
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages python-whois")
try:
    import argparse
    from argparse import RawTextHelpFormatter
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse")

parser = argparse.ArgumentParser(description="Whois Information Search", 
                                 formatter_class=RawTextHelpFormatter, 
                                 epilog="Examples:\n"
                                        "./whois_search.py --url duckduckgo.com")
parser.add_argument("--url", "-u", help="url, ex: duckduckgo.com", required=True)
args = parser.parse_args()

def start():
    print(whois(argv_input))

if __name__ == "__main__":
    argv_input = sys.argv[2]
    start()
